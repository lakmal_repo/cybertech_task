<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailSchedulerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_scheduler', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email_alias');
            $table->string('email_subject');
            $table->text('email_body')->nullable();
            $table->text('attache_file_name')->nullable();
            $table->dateTime('send_date')->nullable();
            $table->text('email_batch_id')->nullable();
            $table->tinyInteger('is_schedule')->default('0');
            $table->unsignedBigInteger('user_id')->nullable();

            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')
                ->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_scheduler');
    }
}
