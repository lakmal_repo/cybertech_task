<?php

namespace Modules\EmailScheduler\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmailScheduler extends Model
{
    use HasFactory;

    protected $table = "email_scheduler";

    protected $fillable = ['email_alias','email_subject','email_body','attache_file_name','send_date','email_batch_id','is_schedule','user_id'];

    protected static function newFactory()
    {
        return \Modules\EmailScheduler\Database\factories\EmailSchedulerFactory::new();
    }
}
