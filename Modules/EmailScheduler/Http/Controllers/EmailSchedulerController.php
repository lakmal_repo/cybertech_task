<?php

namespace Modules\EmailScheduler\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Modules\EmailScheduler\Entities\EmailScheduler;

class EmailSchedulerController extends Controller
{
    public function __construct()
    {
//        error_reporting(E_ALL ^ E_NOTICE);
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $schedule_emails = EmailScheduler::all();

        return view('emailscheduler::index',compact('schedule_emails'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $batches_list = DB::table('recipient_emails')->where('user_id',auth()->user()->id)->groupBy('batch_no')->get();
        return view('emailscheduler::add_schedule_view',compact('batches_list'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $validateData = Validator::make($request->all(),[
            'email_alias' => 'required',
            'email_subject' => 'required',
            'email_body' => 'required',
            'send_date' => 'required',
            'email_batches' => 'required'
        ]);

        if ($validateData->fails()) {
            return response()->json([
                'status'   => false,
                'message'  => $validateData->getMessageBag()
            ],401);
        }
        $attach_name = "";
        if($request->file()) {
            $fileName = time().'_'.$request->email_attach->getClientOriginalName();
            $filePath = $request->file('email_attach')->storeAs('uploads', $fileName, 'public');

            $attach_name = '/storage/' . $filePath;
        }


        $data = [
            'email_alias' => $request->email_alias,
            'email_subject' => $request->email_subject,
            'email_body' => $request->email_body,
            'attache_file_name' => $attach_name,
            'send_date' => $request->send_date,
            'email_batch_id' => json_encode($request->email_batches),
            'user_id' => auth()->user()->id,
        ];
        $res = EmailScheduler::create($data);

        if($res){

            $resp_msg = ['status' => true,'message' => 'Your schedule successfully added.'];
            return response()->json($resp_msg,200);
        }
    }


    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('emailscheduler::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('emailscheduler::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
