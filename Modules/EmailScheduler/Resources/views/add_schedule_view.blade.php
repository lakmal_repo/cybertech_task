@extends('emailmanager::layouts.master')
@section('title','Email Master | List')
@section('content')
    <section class="content">

        <!-- Default box -->
        <form method="post" action="{{route('emailscheduler.store')}}" enctype="multipart/form-data" id="schedule-form">
            @csrf
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title" >Add New Schedule</h3>
                <div class="box-tools pull-right">
                    <a href="{{route('emailscheduler.index')}}" class="btn btn-info btn-sm" data-toggle="tooltip" title="">
                        <i class="icon-backward2"></i> Back
                    </a>
                </div>
            </div>
            <div class="box-body" id="customer_add_view">

                    <div class="col-md-offset-3 col-md-6">
                        <div class="form-group">
                            <label>Email Alias</label>
                            <input type="text" name="email_alias" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Email Subject</label>
                            <input type="text" name="email_subject" class="form-control">
                        </div>
                        <div class="form-group" id="email_body_container">
                            <label>Email Body</label>
                            <textarea class="form-control" name="email_body" id="email_body"></textarea>
{{--                            <span id="word_count"></span>--}}
                            <span id="word_left"></span>
                        </div>
                        <div class="form-group">
                            <label>Attachment</label>
                            <input type="file" name="email_attach" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Send Date</label>
                            <input type="datetime-local" name="send_date" class="form-control" value="@php echo date('Y-m-d\TH:i'); @endphp">
                        </div>
                        <div class="form-group">
                            <label>Select Batches</label>
                            <select name="email_batches[]" class="form-control" multiple>
                                @foreach($batches_list AS $bList)
                                    <option value="{{$bList->batch_no}}">{{$bList->batch_no}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="row">

                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success pull-right" ><i class="icon-enter3"></i> Submit</button>

                        <button class="btn btn-danger pull-left" style="margin-right: 5px"><i class="icon-cancel-square"></i> Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        </form>
        <!-- /.box -->

    </section>

    <script>
        var wordLimit = 5000;
        var words = 0;
        var jqContainer = $("#email_body_container");
        var jqElt = $("#email_body");

        function charLimit()
        {
            var words = 0;
            var wordmatch = jqElt.val().match(/[^\s]+\s+/g);
            words = wordmatch?wordmatch.length:0;

            if (words > wordLimit) {
                var trimmed = jqElt.val().split(/(?=[^\s]\s+)/, wordLimit).join("");
                var lastChar = jqElt.val()[trimmed.length];
                jqElt.val(trimmed + lastChar);
            }
            // $('#word_count', jqContainer).text(words);
            $('#word_left', jqContainer).text(Math.max(wordLimit-words, 0));
        }
        $(document).ready(function () {
            jqElt.on("keyup", charLimit);
            charLimit();

            $(document).on('submit', '#schedule-form', function(event){
                event.preventDefault();
                var form = $(this);
                $.ajax({
                    url: form.attr('action'),
                    type: form.attr('method'),
                    data:new FormData(this),
                    contentType:false,
                    processData:false,
                    success:function(data)
                    {
                        $('#schedule-form')[0].reset();
                        location.reload();

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if(typeof jqXHR.responseJSON.message == "undefined"){
                            $('#schedule-form')[0].reset();
                            location.reload();
                        }
                        var errors = jqXHR.responseJSON.message;


                        var errorList = "";

                        $.each(errors, function (i, error) {
                            errorList +=  error ;
                        })

                        errorList +=""

                        alert(errorList);
                    }
                });
            });
        })

    </script>
@endsection
