@extends('emailscheduler::layouts.master')

@section('title','Email Scheduler | List')
@section('content')
    <section class="content">
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
    @endif
    <!-- Default box -->
        <div class="box box-info">
            <div class="box-header with-border">
                <div class="col-md-3">
                    <h3 class="box-title" >Recipients Email List</h3>
                </div>

                <div class="col-md-offset-5 col-md-4">
{{--                    <a href="{{asset('bulk_email_temp.xlsx')}}" class="btn btn-primary pull-right" style="margin-left: 5px;">Download Excel Template</a>--}}
                    <a href="{{route('emailscheduler.create')}}" class="btn btn-info pull-right" onclick="">Add New Schedule</a>
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
                <div class="col-md-12">
                    <table class="table table-bordered table-hover" id="ID_EmpList_tbl">
                        <thead>
                        <tr>
                            <th >Id</th>
                            <th style="" >Email Alias</th>
                            <th >Subject</th>
                            <th >Send Date</th>
                        </tr>
                        </thead>
                        <tbody id="" >
                        @foreach($schedule_emails AS $list)
                            <tr>
                                <td>{{$list->id}}</td>
                                <td>{{$list->email_alias}}</td>
                                <td>{{$list->email_subject}}</td>
                                <td>{{$list->send_date}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->

        </div>
        <!-- /.box -->

    </section>
@endsection
