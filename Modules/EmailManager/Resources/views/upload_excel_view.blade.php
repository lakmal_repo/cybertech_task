@extends('emailmanager::layouts.master')
@section('title','Email Master | List')
@section('content')
    <section class="content">

        <!-- Default box -->
        <form method="post" action="{{route('emailmanager.importRecipientEmail')}}" enctype="multipart/form-data">
            @csrf
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title" >Upload Recipient Emails</h3>
                <div class="box-tools pull-right">
                    <a href="{{route('emailmanager.index')}}" class="btn btn-info btn-sm" data-toggle="tooltip" title="">
                        <i class="icon-backward2"></i> Back
                    </a>
                </div>
            </div>
            <div class="box-body" id="customer_add_view">

                    <div class="row">
                        <div class="col-md-offset-3 col-md-6">
                            <label>Choose Filed Recipient Excel file to upload...</label>
                            <input type="file" name="file" class="form-control">
                        </div>
                    </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="row">

                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success pull-right" ><i class="icon-enter3"></i> Submit</button>

                        <button class="btn btn-danger pull-left" style="margin-right: 5px"><i class="icon-cancel-square"></i> Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        </form>
        <!-- /.box -->

    </section>
@endsection
