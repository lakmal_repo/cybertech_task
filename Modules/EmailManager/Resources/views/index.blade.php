@extends('emailmanager::layouts.master')
@section('title','Email Master | List')
@section('content')
    <section class="content">
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        <!-- Default box -->
        <div class="box box-info">
            <div class="box-header with-border">
                <div class="col-md-3">
                    <h3 class="box-title" >Recipients Email List</h3>
                </div>

                <div class="col-md-offset-5 col-md-4">
                    <a href="{{asset('bulk_email_temp.xlsx')}}" class="btn btn-primary pull-right" style="margin-left: 5px;">Download Excel Template</a>
                    <a href="{{route('emailmanager.uploadExcelView')}}" class="btn btn-info pull-right" onclick="">Upload Emails</a>
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
                <div class="col-md-12">
                    <table class="table table-bordered table-hover" id="ID_EmpList_tbl">
                        <thead>
                        <tr>
                            <th >Id</th>
                            <th style="" >Name</th>
                            <th >Number</th>
                            <th >Email Address</th>
                        </tr>
                        </thead>
                        <tbody id="" >
                        @foreach($recipient_emails AS $list)
                            <tr>
                                <td>{{$list->id}}</td>
                                <td>{{$list->name}}</td>
                                <td>{{$list->number}}</td>
                                <td>{{$list->email_address}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->

        </div>
        <!-- /.box -->

    </section>
@endsection
