<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipientEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipient_emails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('excel_id');
            $table->string('batch_no',199);
            $table->string('name',199);
            $table->char('number',15)->nullable();
            $table->string('email_address',199);
            $table->unsignedBigInteger('user_id')->nullable();

            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')
                ->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipient_emails');
    }
}
