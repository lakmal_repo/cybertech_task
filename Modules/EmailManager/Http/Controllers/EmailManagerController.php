<?php

namespace Modules\EmailManager\Http\Controllers;

use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
//use Maatwebsite\Excel\Excel;
use Illuminate\Support\Facades\Log;
use Modules\EmailManager\Entities\RecipientEmail;
use Excel;
use App\Imports\EmailImport;

class EmailManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $recipient_emails = RecipientEmail::all();
        return view('emailmanager::index',compact('recipient_emails'));
    }

    /**
     * @return Application|Factory|View
     */
    public function uploadExcelView(){

        return view('emailmanager::upload_excel_view');
    }

    /**
     * @param Request $request
     * @return string
     */
    public function importRecipientEmail(Request $request){

        try {

            Excel::import(new EmailImport,$request->file);
            return redirect()->route('emailmanager.index')
                ->with('success','Excel Uploaded successfully');
        }catch (Exception $e){

            Log::info('ExcelUpError = '.$e->getMessage());
        }

    }

}
