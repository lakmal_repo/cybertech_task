<?php

namespace Modules\EmailManager\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RecipientEmail extends Model
{
    use HasFactory;

    protected $fillable = ['batch_no','name','number','email_address','user_id'];

    protected static function newFactory()
    {
        return \Modules\EmailManager\Database\factories\RecipientEmailFactory::new();
    }
}
