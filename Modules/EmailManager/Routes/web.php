<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('emailmanager')->name('emailmanager.')->group(function() {
    Route::get('/', 'EmailManagerController@index')->name('index');
    Route::get('/upload-excel-view', 'EmailManagerController@uploadExcelView')->name('uploadExcelView');
    Route::post('/import-recipient-excel', 'EmailManagerController@importRecipientEmail')->name('importRecipientEmail');
});
