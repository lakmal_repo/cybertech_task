/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.4.17-MariaDB : Database - cybertech_task_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cybertech_task_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `cybertech_task_db`;

/*Table structure for table `email_scheduler` */

DROP TABLE IF EXISTS `email_scheduler`;

CREATE TABLE `email_scheduler` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attache_file_name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `send_date` datetime DEFAULT NULL,
  `email_batch_id` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_schedule` tinyint(4) NOT NULL DEFAULT 0,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `email_scheduler_user_id_foreign` (`user_id`),
  CONSTRAINT `email_scheduler_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `email_scheduler` */

insert  into `email_scheduler`(`id`,`email_alias`,`email_subject`,`email_body`,`attache_file_name`,`send_date`,`email_batch_id`,`is_schedule`,`user_id`,`created_at`,`updated_at`) values 
(1,'Marketing email','Hooray! This is the email subject','(text editor [nice editor will be suggested] - max words count should be\r\n5000)','','2021-03-20 12:35:00','[\"2\",\"3\"]',0,1,'2021-03-20 12:35:46','2021-03-20 12:35:46'),
(2,'Marketing email','Hooray! This is the email subject','(text editor [nice editor will be suggested] - max words count should be\r\n5000)','','2021-03-20 12:35:00','[\"2\",\"3\"]',0,1,'2021-03-20 12:42:06','2021-03-20 12:42:06'),
(3,'Marketing email','Hooray! This is the email subject','(text editor [nice editor will be suggested] - max words count should be\r\n5000)','','2021-03-20 12:35:00','[\"2\",\"3\"]',0,1,'2021-03-20 12:45:09','2021-03-20 12:45:09'),
(4,'Marketing email','Hooray! This is the email subject','(text editor [nice editor will be suggested] - max words count should be\r\n5000)','','2021-03-22 12:50:00','[\"1\",\"3\"]',0,1,'2021-03-20 12:47:44','2021-03-20 12:47:44'),
(5,'Marketing email','Hooray! This is the email subject','(text editor [nice editor will be suggested] - max words count should be\r\n5000)','','2021-03-22 12:50:00','[\"1\",\"3\"]',0,1,'2021-03-20 12:48:06','2021-03-20 12:48:06'),
(6,'support@beautiesessentials.com','support@beautiesessentials.com','gfdh 5646fdh 65+hdgd df6g+5  gdsfgd5+6dfg yghg dfg sdf','','2021-03-20 12:50:00','[\"2\",\"3\"]',0,1,'2021-03-20 12:50:37','2021-03-20 12:50:37'),
(7,'bengoo@gmail.com','bengoo@gmail.com','$(\'#schedule-form\')[0].reset();fdsfsdfsf fsdf fsdf','','2021-03-20 12:52:00','[\"1\",\"3\"]',0,1,'2021-03-20 12:52:45','2021-03-20 12:52:45'),
(8,'Oils@gmail.com','Oils@gmail.com','Silver Developer in the organization primarily allocating to 04 hours per day for a specific development. Hence the organization quoted that the evaluation task will take maximum of 10 hours to complete. (So we are expecting you to complete this task within the next 48 hours) According to that please find the deadline given below for your reference and mention the maximum hours that you took for the development.','/storage/uploads/1616246104_image (2).png','2021-03-20 13:16:00','[\"2\",\"3\"]',0,1,'2021-03-20 13:15:04','2021-03-20 13:15:04');

/*Table structure for table `failed_jobs` */

DROP TABLE IF EXISTS `failed_jobs`;

CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `failed_jobs` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2014_10_12_000000_create_users_table',1),
(2,'2014_10_12_100000_create_password_resets_table',1),
(3,'2019_08_19_000000_create_failed_jobs_table',1),
(4,'2021_03_20_053816_create_recipient_emails_table',1),
(5,'2021_03_20_054304_create_email_scheduler_table',1);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `recipient_emails` */

DROP TABLE IF EXISTS `recipient_emails`;

CREATE TABLE `recipient_emails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `excel_id` varchar(199) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `batch_no` varchar(199) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(199) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` char(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(199) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recipient_emails_user_id_foreign` (`user_id`),
  CONSTRAINT `recipient_emails_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `recipient_emails` */

insert  into `recipient_emails`(`id`,`excel_id`,`batch_no`,`name`,`number`,`email_address`,`user_id`,`created_at`,`updated_at`) values 
(18,NULL,'1','Name','Number','Email Address',1,'2021-03-20 10:11:23','2021-03-20 10:11:23'),
(19,NULL,'1','test1','1234545132','test1@email.com',1,'2021-03-20 10:11:23','2021-03-20 10:11:23'),
(20,NULL,'1','test2','718569243','test2@email.com',1,'2021-03-20 10:11:23','2021-03-20 10:11:23'),
(21,NULL,'1','test3','778546213','test3@email.com',1,'2021-03-20 10:11:23','2021-03-20 10:11:23'),
(22,NULL,'2','Name','Number','Email Address',1,'2021-03-20 10:21:28','2021-03-20 10:21:28'),
(23,NULL,'2','test1','1234545132','test1@email.com',1,'2021-03-20 10:21:28','2021-03-20 10:21:28'),
(24,NULL,'2','test2','718569243','test2@email.com',1,'2021-03-20 10:21:28','2021-03-20 10:21:28'),
(25,NULL,'2','test3','778546213','test3@email.com',1,'2021-03-20 10:21:28','2021-03-20 10:21:28'),
(26,NULL,'3','Name','Number','Email Address',1,'2021-03-20 10:22:02','2021-03-20 10:22:02'),
(27,NULL,'3','test1','1234545132','test1@email.com',1,'2021-03-20 10:22:02','2021-03-20 10:22:02'),
(28,NULL,'3','test2','718569243','test2@email.com',1,'2021-03-20 10:22:02','2021-03-20 10:22:02'),
(29,NULL,'3','test3','778546213','test3@email.com',1,'2021-03-20 10:22:02','2021-03-20 10:22:02');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`remember_token`,`created_at`,`updated_at`) values 
(1,'Dinesh Wijethunga','admin@admin.com',NULL,'$2y$10$IypZu.1y8h2CrdzIglolBuVEaDpbDnPPBwjc.KNttNbPAkC/awvMO',NULL,'2021-03-20 06:30:11','2021-03-20 06:30:11'),
(2,'cybertech','admin1@admin.com',NULL,'$2y$10$IypZu.1y8h2CrdzIglolBuVEaDpbDnPPBwjc.KNttNbPAkC/awvMO',NULL,'2021-03-20 18:48:31','2021-03-20 18:48:34');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
