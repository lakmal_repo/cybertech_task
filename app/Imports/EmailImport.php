<?php

namespace App\Imports;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\ToCollection;
use Modules\EmailManager\Entities\RecipientEmail;
//use Maatwebsite\Excel\Concerns\ToModel;
//use Maatwebsite\Excel\Concerns\WithHeadingRow;

class EmailImport implements ToCollection
{
    /**
     * @param Collection $row
     *
     * @return Model|null
     */
    public function collection(Collection $rows)
    {
        $last_batch_id = DB::table('recipient_emails')->orderBy('id','desc')->first();

        if(empty($last_batch_id)) $last_batch_no=1;else $last_batch_no = ((int)($last_batch_id->batch_no)+1);

        $excel_id = '';
        $name = '';
        $number = '';
        $email_address = '';

        try {

            $res = '';
            foreach ($rows as $row)
            {
                if(isset($row[0])) $excel_id = $row[0];
                if(isset($row[1])) $name = $row[1];
                if(isset($row[2])) $number = $row[2];
                if(isset($row[3])) $email_address = $row[3];

               $res = RecipientEmail::create([
                    'batch_no' => $last_batch_no,
                    'excel_id' => $excel_id,
                    'name' => $name,
                    'number' => $number,
                    'email_address' => $email_address,
                    'user_id' => auth()->user()->id,
                ]);
            }
            return $res;
        }catch (Exception $e){

            Log::info('ExcelUpError = '.$e->getMessage());
        }
    }
}
